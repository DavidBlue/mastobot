/**
 *  MastoBot, the Mastodon client library
 *  
 *  @file       lib/mastobot.js
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       May 2022
 *  
 */

'use strict';

// const {
//   APIError,
// } = require('./util');
const { MastoBotAPI } = require('./mastobotAPI');


/**
 *  MastoBot -- Smart client machinery
 */
class MastoBot extends MastoBotAPI {
  /**
   *  @param {string}   api_url   URL of the instance to connect to; should
   *    include "https:" but not "api/v1/"
   *
   *  @param {Object}   options
   *  @param {string=}  options.api_token   User access token
   *  @param {string=}  options.userAgent   to override the user-agent provided
   *  @param {string=}  options.client_id     oAuth client ID
   *  @param {string=}  options.client_secret oAuth client secret
   */
  constructor(api_url = 'https://botsin.space/api/', options = {}) {
    super(api_url, options);

  }
}

Object.assign(exports, {
  MastoBot,
});
