/**
 *  Mastobot Utility Library
 *
 *  @author Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date   May 2022
 */

'use strict';

/**
 *  API Error to enwrap any errors coming from the Mastodon API
 *
 *  @param {string} message       Human-readable error message
 *  @param {string} data          Object containing additional fields to attach:
 *  @param {string} data.code        - the MastoBot error code, if such exists
 *  @param {string} data.statusCode  - the HTTP status code
 *  @param {string} data.statusText  - the HTTP status text
 */
class APIError extends Error {
  constructor(message, data = {}) {
    super(message);

    this.code = data.code || data.statusCode || 'unknown';
    this.statusCode = '0';
    this.statusText = 'unspecified';

    Object.assign(this, data);
  }
}


/**
 *  Replace colon-parameters in the path with values from `params`. Keys
 *    will be removed from `params` as they are used, to avoid duplicating
 *    in the query string/POST body.
 *
 *  @param  {string} path   Original path to the API endpoint
 *  @param  {Object<string:string>} params Parameters to fill in. Properties will be
 *    removed as they are consumed; this object will be mutated
 *
 *  @return {string}        New path with parameters filled in
 */
function populateKeysInPath(path, params) {
  return path.replace(/:(\w+)/g, key => {
    const val = params[key.replace(/^:/, '')];
    if (typeof val === 'undefined')
      throw new TypeError(`MastoBot: Missing required path parameter '${key}', value was (${typeof val}) '${val}'`);
    const retval = encodeURIComponent(val);
    delete params[key];
    return retval;
  });
}

/**
 *  Form-encode parameters for transmission in a request body or query string
 *
 *  @param  {object<string:string>}       params  Object to be transmitted
 *  @return {string}          url-encoded string suitable for transmission
 */
function formEncodeParameters(params) {
  return Object.keys(params)
  .filter(key => {
    return typeof params[key] !== 'undefined';
  })
  .map(key => {
    const value = params[key];

    // encode an Array as a series of `key[]=val1&key[]=val2`
    if (Array.isArray(value))
      return value
        .map(v => encodeURIComponent(key + '[]') + '=' + encodeURIComponent(v))
        .join('&');

    // encode an Object as a series of `key[prop1]=val1&key[prop2]=val2`
    if (typeof value === 'object' && !!value)
      return Object.entries(value)
        .map(([k, v]) => encodeURIComponent(key + `[${k}]`) + '=' + encodeURIComponent(v))
        .join('&');

    // encode a simple value as `key=value`
    return encodeURIComponent(key) + '=' + encodeURIComponent(value);
  })
  .join('&');
}

/** 
 *  Parse an http Link header into an Object
 *
 *  @param {string} linkHeader  content of the Link header to parse
 *  
 *  @return {object<string:string>}  Returned object is a map of url strings indexed by relation
 */
function parseLinkHeader(linkHeader) {
  if (!linkHeader)
    return;

  const link = {};

  const linkParts = linkHeader.split(/,\s+/);

  for (const l of linkParts) {
    const [, href, rel] = l.match(/<(\S+)>; rel="(\w+)"/);

    if (href && rel)
      link[rel] = href;
  }

  return link;
}

/**
 *  Calculate the length of a toot
 *  - https URLs are 23 characters
 *  - "@user@domain" is "@user" long
 *
 *  @param  {string} toot         Text of the Toot
 *  @param  {String} spoiler_text Spoiler text, if any
 *  @return {number}              Character length of the Toot
 */
function tootLength(toot, spoiler_text = '') {
  if (typeof toot !== 'string')
    throw new TypeError('toot parameter must be a single string');
  if (typeof spoiler_text !== 'string')
    throw new TypeError('spoiler_text parameter must be a single string');
  return (spoiler_text + toot)
    .replace(/https:\S+/, 'x'.repeat(23))
    .replace(/(@\S+)@\S+/, '$1')
    .length;
}


Object.assign(exports, {
  APIError,
  populateKeysInPath,
  formEncodeParameters,
  parseLinkHeader,
  tootLength,
});
