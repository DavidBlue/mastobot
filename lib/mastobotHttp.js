/**
 *  MastoBot HTTP Helper class
 *
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       May 2022
 */

'use strict';

const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));

const {
  APIError,
  populateKeysInPath,
  formEncodeParameters,
  parseLinkHeader,
} = require('./util');


/**
 *  @typedef  {Object}    APIResponse
 *  @global
 *  @template T
 *  @property {string}    statusCode    HTTP status code
 *  @property {string}    statusText    HTTP Status
 *  @property {string}    contentType   Mime type of the reply
 *  @property {T|string}  data          Server response; type depends on contentType
 */


/**
 *  MastoBotHTTP -- HTTP Request Helpers
 *
 */
class MastoBotHttp {
  /**
   *  @param {string}   api_url   URL of the instance to connect to; should
   *    include "https:" but not "api/v1/"
   *
   *  @param {Object}   options
   *  @param {string=}  options.api_token   User access token
   *  @param {string=}  options.userAgent   to override the user-agent provided
   *    to the instance
   */
  constructor(api_url = 'https://botsin.space/', options = {}) {

    this._api = {
      url: api_url.replace(/(api\/)?(v\d+\/)?$/, ''),  // strip "api/v1" etc. 
      token: options.api_token || '',
      userAgent: options.userAgent || 'mastobot-client-' + require('../package.json').version,
    };
  }

  /**
   *  Send a request to the Mastodon API
   *
   *  @param  {string} method  Request method; 'get', 'post', etc.
   *  @param  {string} path    API endpoint path
   *  @param  {Object} options Additional options, eg. headers
   *  @return {APIResponse<*>}   Contents of `data` property will
   *    depend on the specific endpoint
   */
  async _request(method, path, options = {}) {
    // Allow passing a complete URL; otherwise prepend the api_url
    const url = path.startsWith('http')
      ? path
      : this._api.url + path;

    const headers = Object.assign({
      'Accept': '*/*',
      'User-Agent': this._api.userAgent,
    }, options.headers);


    // If the request isn't flagged "public" and the request is going to the
    // active instance, then attach the api_token
    if (!options.public && url.startsWith(this._api.url))
      headers.Authorization = this._api.token ? ('Bearer ' + this._api.token) : undefined;

    const response = await fetch(url, {
      method,
      headers,
      body: options.body,
    });

    const [contentType] = response.headers.get('content-type').split(/;\s*/);
    const link = parseLinkHeader(response.headers.get('link'));

    const apiResponse = {
      statusCode: response.status,
      statusText: response.statusText,
      contentType,
      data:       undefined,
    };

    if (link)
      apiResponse.link = link;

    switch (contentType) {
      case 'application/json': 
        apiResponse.data = await response.json();
        break;

      case 'text/plain': 
        apiResponse.data = await response.text();
        break;

      default: 
        apiResponse.code = 'EBADTYPE';
        apiResponse.data = await response.text();

        throw new APIError('Unexpected return type', apiResponse);
    }

    if (response.status >= 200 && response.status <= 299) {
      return apiResponse;
    }
    else {
      throw new APIError(response.statusText, apiResponse);
    }
  }

  /**
   *  Make a GET request to the Mastodon API
   *
   *  @param  {string} path    API endpoint
   *  @param  {Object} data    Parameters to send in the path/query string
   *  @param  {Object} options Additional options to set (eg. headers)
   *  @return {APIResponse<*>}   Contents of `data` property will
   *    depend on the specific endpoint
   */
  async get(path, data = {}, options = {}) {
    const params = Object.assign({}, data);

    // First, replace :parameters in the path using keys from data:
    path = populateKeysInPath(path, params);

    // Then assemble the query string from the remaining parameters
    const search = formEncodeParameters(params);

    const url = [path, search].join('?');

    return this._request('get', url, options);
  }

  /**
   *  Make a POST request to the Mastodon API
   *
   *  If `data` contains a property named 'file', it will be send with
   *    content-type multipart/form-data, otherwise it will be sent as
   *    application/x-www-form-urlencoded
   *
   *  @param  {string} path    API endpoint
   *  @param  {Object} data    Parameters to send in the path or request
   *    body
   *
   *  @param  {Object} options Additional options to set (eg. headers)
   *  @return {APIResponse<*>}   Contents of `data` property will
   *    depend on the specific endpoint
   */
  async post(path, data = {}, options = {}) {
    const params = Object.assign({}, data);

    // First, replace :id in the path using keys from data:
    path = populateKeysInPath(path, params);

    // Encode the body parts
    if (params.file || (options.headers && options.headers['content-type'] === 'multipart/form-data')) {
      options.headers = Object.assign(options.headers || {}, {
        'content-type': 'multipart/form-data',
      });
      options.body = params;
    }
    else {
      options.headers = Object.assign(options.headers || {}, {
        'content-type': 'application/x-www-form-urlencoded',
      });
      options.body = formEncodeParameters(params);
    }

    return this._request('post', path, options);
  }

  /**
   *  Make a PATCH request to the Mastodon API
   *
   *  @param  {string} path    API endpoint
   *  @param  {Object} data    Parameters to send in the path or request
   *    body
   *
   *  @param  {Object} options Additional options to set (eg. headers)
   *  @return {APIResponse<*>}   Contents of `data` property will
   *    depend on the specific endpoint
   */
  async patch(path, data = {}, options = {}) {
    const params = Object.assign({}, data);

    // First, replace :id in the path using keys from data:
    path = populateKeysInPath(path, params);

    // Encode the body parts
    if (params.file || (options.headers && options.headers['content-type'] === 'multipart/form-data')) {
      options.headers = Object.assign(options.headers || {}, {
        'content-type': 'multipart/form-data',
      });
      options.body = params;
    }
    else {
      options.headers = Object.assign(options.headers || {}, {
        'content-type': 'application/x-www-form-urlencoded',
      });
      options.body = formEncodeParameters(params);
    }

    return this._request('patch', path, options);
  }

  /**
   *  Make a DELETE request to the Mastodon API
   *
   *  @param  {string} path    API endpoint
   *  @param  {Object} data    Parameters to send in the path/query string
   *  @param  {Object} options Additional options to set (eg. headers)
   *  @return {APIResponse<*>}   Contents of `data` property will
   *    depend on the specific endpoint
   */
  async delete(path, data = {}, options = {}) {
    const params = Object.assign({}, data);

    // First, replace :id in the path using keys from data:
    path = populateKeysInPath(path, params);

    // Encode the body parts
    if (params.file || (options.headers && options.headers['content-type'] === 'multipart/form-data')) {
      options.headers = Object.assign(options.headers || {}, {
        'content-type': 'multipart/form-data',
      });
      options.body = params;
    }
    else {
      options.headers = Object.assign(options.headers || {}, {
        'content-type': 'application/x-www-form-urlencoded',
      });
      options.body = formEncodeParameters(params);
    }

    return this._request('delete', path, options);
  }

  /**
   *  Make a PUT request to the Mastodon API
   *
   *  If `data` contains a property named 'file', it will be send with
   *    content-type multipart/form-data, otherwise it will be sent as
   *    application/x-www-form-urlencoded
   *
   *  @param  {string} path    API endpoint
   *  @param  {Object} data    Parameters to send in the path or request
   *    body
   *
   *  @param  {Object} options Additional options to set (eg. headers)
   *  @return {APIResponse<*>}   Contents of `data` property will
   *    depend on the specific endpoint
   */
  async put(path, data = {}, options = {}) {
    const params = Object.assign({}, data);

    // First, replace :id in the path using keys from data:
    path = populateKeysInPath(path, params);

    // Encode the body parts
    if (params.file || (options.headers && options.headers['content-type'] === 'multipart/form-data')) {
      options.headers = Object.assign(options.headers || {}, {
        'content-type': 'multipart/form-data',
      });
      options.body = params;
    }
    else {
      options.headers = Object.assign(options.headers || {}, {
        'content-type': 'application/x-www-form-urlencoded',
      });
      options.body = formEncodeParameters(params);
    }

    return this._request('put', path, options);
  }


  /**
   *  Set a new API token
   *
   *  @param {string} api_token New token. This will be passed with all
   *    future requests.
   */
  setApiToken(api_token) {
    this._api.token = api_token;
  }
}

Object.assign(exports, {
  MastoBotHttp,
});
